package JsonPathExamples;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class JsonPathInro {
	
	@Test
	public void jsonPathDemo()
	{
		String json = "{\"firstname\" : \"Jim\",\r\n"
				+ "    \"lastname\" : \"Brown\",\r\n"
				+ "    \"totalprice\" : 111,\r\n"
				+ "    \"depositpaid\" : true,\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2018-01-01\",\r\n"
				+ "        \"checkout\" : \"2019-01-01\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\" : \"Breakfast\"}";
		
		//Get JsonPath instance of given json document
		JsonPath jsonPath = new JsonPath(json);
		String checkinDate=jsonPath.getString("bookingdates.checkin");
		System.out.println(checkinDate);
		String additionalneeds=jsonPath.getString("additionalneeds");
		System.out.println(additionalneeds);
		
		Object firstname=jsonPath.get("firstname");
//		System.out.println(firstname);
		
		int fname=jsonPath.get("firstname1");//firstname1 key is not present
		System.out.println(fname);
		
		
	}

}
