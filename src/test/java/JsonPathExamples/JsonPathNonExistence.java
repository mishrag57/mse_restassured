package JsonPathExamples;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class JsonPathNonExistence {
	
	@Test
	public void jsonPathDemo()
	{
		String json = "{\"firstname\" : \"Jim\",\r\n"
				+ "    \"lastname\" : \"Brown\",\r\n"
				+ "    \"totalprice\" : 111,\r\n"
				+ "    \"depositpaid\" : true,\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2018-01-01\",\r\n"
				+ "        \"checkout\" : \"2019-01-01\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\" : \"Breakfast\"}";
		
		//Get JsonPath instance of given json document
		JsonPath jsonPath = new JsonPath(json);
		 System.out.println((Object)jsonPath.get("$"));
		 System.out.println(jsonPath.getString("$"));
		 System.out.println((Object)jsonPath.get());
		 System.out.println(jsonPath.getString(""));
		
		
	}

}
