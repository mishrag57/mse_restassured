package RestfulBooker;

import io.restassured.RestAssured;

public class DeleteBooking {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RestAssured
		//Construct request
			.given()
				.log()
				.all()
				.baseUri("https://restful-booker.herokuapp.com/")
				.basePath("booking/1")
				.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
				//Hit Request
			.when()
				.delete()
				//Validate Response
			.then()
			.log()
			.all()
				.assertThat()
				.statusCode(201);

	}

}
