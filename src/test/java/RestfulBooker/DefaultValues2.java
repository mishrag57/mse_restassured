package RestfulBooker;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class DefaultValues2 {
	
	@Test
	public void createBooking1()
	{
		// SetUp The Request
				RestAssured.given()
						.basePath("auth") //Here we have passed "auth" as basePath
						.body("{\r\n"
								+ "    \"username\" : \"admin\",\r\n"
								+ "    \"password\" : \"password123\"\r\n"
								+ "}")
						.contentType(ContentType.JSON)
						// Hit Request and Get Response
						.post()
						// Validate the Response
						.then().log().all();
	}
	
	/*
	 * @Test public void createBooking2() { // SetUp The Request
	 * RestAssured.given().log().all() .body("{\r\n" +
	 * "   \"firstname\" : \"Kima\",\r\n" + "    \"lastname\" : \"Brown\",\r\n" +
	 * "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n" +
	 * "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2021-03-01\",\r\n"
	 * + "        \"checkout\" : \"2021-03-01\"\r\n" + "    },\r\n" +
	 * "    \"additionalneeds\" : \"Lunch\"\r\n" + "}")
	 * .contentType(ContentType.JSON) // Hit Request and Get Response .post() //
	 * Validate the Response .then().log().all().statusCode(200); }
	 */

}
