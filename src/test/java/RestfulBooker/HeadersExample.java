package RestfulBooker;

import java.util.*; 

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;

public class HeadersExample {
	
	@Test(enabled = true)
	public void passHeader6()
	{
		Header header = new Header ("Header1", "Value1");
		Header header1 = new Header ("Header2", "Value2");
		Header header2 = new Header ("Header3", "Value3");
		
		List<Header> allHeaders = new ArrayList<>();
		allHeaders.add(header);
		allHeaders.add(header1);
		allHeaders.add(header2);
		
		Headers headers = new Headers(allHeaders);
		
		RestAssured
			.given()
			.log()
			.all()
			.headers(headers)
		.when()
			.get();
	}
	
	@Test(enabled = false)
	public void passHeader5()
	{
		Header header = new Header ("Header1", "Value1");
		Headers headers = new Headers(header);
		RestAssured
			.given()
			.log()
			.all()
			.headers(headers)
		.when()
			.get();
	}
	
	
	@Test(enabled = false)
	public void passHeader4()
	{
		HashMap<String, String> hash_map = new HashMap<String, String>(); 
		hash_map.put("H1", "Geeks"); 
	    hash_map.put("H2", "4"); 
	    hash_map.put("H3", "Geeks"); 
	    hash_map.put("H4", "Welcomes"); 
	    hash_map.put("H5", "You"); 
		
		
		RestAssured
			.given()
			.log()
			.all()
			.headers(hash_map)
		.when()
			.get();
	}

	
	@Test(enabled = false)
	public void passHeader3()
	{
		RestAssured
			.given()
			.log()
			.all()
			.headers("H1","V1","H2","V2","H3","V3","H3","V4","H3","V5")
		.when()
			.get();
	}

	
	@Test(enabled = false)
	public void passHeader2()
	{
		Header header = new Header("Header1", "Value1");
		RestAssured
			.given()
			.log()
			.all()
			.header(header)
		.when()
			.get();
	}

	
	
	@Test(enabled = false)
	public void passHeader1()
	{
		RestAssured
			.given()
			.log()
			.all()
			.header("Header1","Value1")
			.header("Header2","Value1")
			.header("Header2","Value2")
			.header("Header2","Value3")
			.header("Header2","Value4")
			.header("Header3","Value1","Value2","Value3")//we can pass mulptile Values in Both Ways
		.when()
			.get();
	}
	
	
}
