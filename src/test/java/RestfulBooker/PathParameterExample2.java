package RestfulBooker;

import io.restassured.RestAssured;

public class PathParameterExample2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RestAssured
			.given()
				.log()
				.all()
			.when()
				.get("https://restful-booker.herokuapp.com/{basePath}/{bookingId}", "booking",2)
			.then()
				.log()
				.all();

	}

}
