package RestfulBooker;

import io.restassured.RestAssured;

public class PathParameterExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RestAssured
			.given()
				.log()
				.all()
				.baseUri("https://restful-booker.herokuapp.com/")
				.basePath("{basePath}/{bookingId}")
				.pathParam("basePath", "booking")
				.pathParam("bookingId", 1)
			.when()
				.get()
			.then()
				.log()
				.all();

	}

}
