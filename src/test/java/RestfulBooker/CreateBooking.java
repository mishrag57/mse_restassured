package RestfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class CreateBooking {

	public static void main(String[] args) {

		// Build Request
		RequestSpecification requestSpecification = RestAssured.given();
		requestSpecification = requestSpecification.log().all();// to get the logs for Request flow

		requestSpecification.baseUri("https://restful-booker.herokuapp.com/");
		requestSpecification.basePath("booking");
		requestSpecification.body("{\r\n" + "   \"firstname\" : \"Jim\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
				+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n" + "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2021-03-01\",\r\n" + "        \"checkout\" : \"2021-03-01\"\r\n"
				+ "    },\r\n" + "    \"additionalneeds\" : \"Lunch\"\r\n" + "}");

		requestSpecification.contentType(ContentType.JSON);
		// Hit Request and Get Response
		Response response = requestSpecification.post();
		// Validate Response
		ValidatableResponse validatableResponse = response.then().log().all();//to get the log for Response flow
		validatableResponse.statusCode(200);
	}

}
