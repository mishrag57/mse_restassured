package RestfulBooker;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class DefaultValues1 {
	
	@BeforeTest
	public void setup()
	{
		RestAssured.baseURI="https://restful-booker.herokuapp.com/";
		RestAssured.basePath="booking";
		System.out.println("In Setup");
		RestAssured.requestSpecification = RestAssured.given().log().all();
		RestAssured.responseSpecification = RestAssured.expect().statusCode(200);
	}
	
	@Test
	public void createBooking1()
	{
		// SetUp The Request
				RestAssured.given()
						.body("{\r\n" + "   \"firstname\" : \"Jim\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
								+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n"
								+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2021-03-01\",\r\n"
								+ "        \"checkout\" : \"2021-03-01\"\r\n" + "    },\r\n"
								+ "    \"additionalneeds\" : \"Lunch\"\r\n" + "}")
						.contentType(ContentType.JSON)
						// Hit Request and Get Response
						.post()
						// Validate the Response
						.then().log().all();
	}
	
	@Test
	public void createBooking2()
	{
		// SetUp The Request
				RestAssured.given()
						.body("{\r\n" + "   \"firstname\" : \"Kima\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
								+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n"
								+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2021-03-01\",\r\n"
								+ "        \"checkout\" : \"2021-03-01\"\r\n" + "    },\r\n"
								+ "    \"additionalneeds\" : \"Lunch\"\r\n" + "}")
						.contentType(ContentType.JSON)
						// Hit Request and Get Response
						.post()
						// Validate the Response
						.then().log().all();
	}
	

}
