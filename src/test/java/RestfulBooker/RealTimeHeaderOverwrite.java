package RestfulBooker;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.config.HeaderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.specification.RequestSpecification;

public class RealTimeHeaderOverwrite {
	
	@Test
	public void realTimeHeaderOverwrite()
	{
		RequestSpecification requestSpecification=RestAssured.given()
				.header("header1","value1");
		RequestSpecification requestSpecification1=RestAssured.given()
				.header("header2","value2")
				.header("header1","value3");
				
		
		RestAssured.given()
		.config(RestAssuredConfig.config()
				.headerConfig(HeaderConfig.headerConfig().overwriteHeadersWithName("header1")))
		.spec(requestSpecification)
		.spec(requestSpecification1)
		.log()
		.all()
	.when()
		.get();
	}

}
