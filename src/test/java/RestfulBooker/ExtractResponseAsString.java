package RestfulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class ExtractResponseAsString {

	public static void main(String[] args) {

		// SetUp The Request
		String responseBody= RestAssured
			.given()
				.baseUri("https://restful-booker.herokuapp.com/")
				.basePath("booking")
				.body("{\r\n" + "   \"firstname\" : \"Jim\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
						+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n"
						+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2021-03-01\",\r\n"
						+ "        \"checkout\" : \"2021-03-01\"\r\n" + "    },\r\n"
						+ "    \"additionalneeds\" : \"Lunch\"\r\n" + "}")
				.contentType(ContentType.JSON)
				// Hit Request and Get Response
				.post()
				// Validate the Response
				.then()
					.extract()
					//.body()
					.asPrettyString();
		
		System.out.println(responseBody);

	}

}
