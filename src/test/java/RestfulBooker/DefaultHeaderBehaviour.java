package RestfulBooker;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.config.HeaderConfig;
import io.restassured.config.RestAssuredConfig;

public class DefaultHeaderBehaviour {
	
	@Test
	public void overwriteHeaderValue()
	{
		RestAssured
			.given()
			.config(RestAssuredConfig.config()
					.headerConfig(HeaderConfig
							.headerConfig()
							.overwriteHeadersWithName("Header1","Header2")))
			
			.config(RestAssuredConfig.config()
					.headerConfig(HeaderConfig
							.headerConfig()
							.mergeHeadersWithName("Header1")))
			
			.header("Header1","Value1")
			.header("Header1","Value2")
			.header("Header2","Value3")
			.header("Header2","Value4")
			.header("Header3","Value5")
			.header("Header3","Value6")
				.log()
				.all()
			.when()
				.get();
	}
	
	
	@Test(enabled = false)
	public void defaultHeaderBehaviour()
	{
		RestAssured
			.given()
			.header("Header1","Value1","Value2")
			.header("Header1","Value2")
				.log()
				.all()
			.when()
				.get();
	}

}
