package RestfulBooker;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class MeasureResponseTime {

	@Test
	public void createBooking1() {
		// SetUp The Request
		Response response = RestAssured.given().log().all().baseUri("https://restful-booker.herokuapp.com/")
				.basePath("booking")
				.body("{\r\n" + "   \"firstname\" : \"Jim\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
						+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n"
						+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2021-03-01\",\r\n"
						+ "        \"checkout\" : \"2021-03-01\"\r\n" + "    },\r\n"
						+ "    \"additionalneeds\" : \"Lunch\"\r\n" + "}")
				.contentType(ContentType.JSON)
				// Hit Request and Get Response
				.post();
		
		response.then().time(Matchers.lessThan(5000L));
		response.then().time(Matchers.greaterThan(2000L));
		response.then().time(Matchers.both(Matchers.greaterThan(2000L))
											.and(Matchers.lessThan(5000L)));
		response.then().time(Matchers.lessThan(5L),TimeUnit.SECONDS);

		long responseTimeMs = response.time();
		System.out.println("Response Time in MS: " + responseTimeMs);

		long responseTimeSeconnds = response.timeIn(TimeUnit.SECONDS);
		System.out.println("Response Time in Second: " + responseTimeSeconnds);

		long responseTimeMs_1 = response.getTime();
		System.out.println("Response Time in MS: " + responseTimeMs_1);

		long responseTimeSeconnds_1 = response.getTimeIn(TimeUnit.SECONDS);
		System.out.println("Response Time in Second: " + responseTimeSeconnds_1);

		// In(TimeUnit.SECONDS);
		// Validate the Response
		// .then().log().all().statusCode(200);
	}

}
