package RestfulBooker;

import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class CreateBookingsWithRequestAndResponseSpecification {

	RequestSpecification requestSpecification;
	ResponseSpecification responseSpecification;

	@BeforeClass
	public void setupRequestSpec() {
		requestSpecification = RestAssured.given();
		requestSpecification.log().all().baseUri("https://restful-booker.herokuapp.com/").basePath("booking")
				.contentType(ContentType.JSON)
				.body("{\r\n" + "   \"firstname\" : \"Jim\",\r\n" + "    \"lastname\" : \"Brown\",\r\n"
						+ "    \"totalprice\" : 111,\r\n" + "    \"depositpaid\" : true,\r\n"
						+ "    \"bookingdates\" : {\r\n" + "        \"checkin\" : \"2021-03-01\",\r\n"
						+ "        \"checkout\" : \"2021-03-01\"\r\n" + "    },\r\n"
						+ "    \"additionalneeds\" : \"Lunch\"\r\n" + "}");

		responseSpecification = RestAssured.expect();
		responseSpecification.statusCode(200).contentType(ContentType.JSON).time(Matchers.lessThan(5000L));
	}

	@Test
	public void createBooking1() {
		// SetUp The Request
		RestAssured.given(requestSpecification, responseSpecification) //.spec(requestSpecification)
				// Hit Request and Get Response
				.post()
				// Validate the Response
				.then().log().all();
				//.spec(responseSpecification);
	}

}
